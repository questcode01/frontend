def LABEL_ID = "questcode-${UUID.randomUUID().toString()}"
podTemplate(
  name: 'questcode',
  namespace: 'devops',
  label: LABEL_ID,
  containers: [
    containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', livenessProbe: containerLivenessProbe(execArgs: '', failureThreshold: 0, initialDelaySeconds: 0, periodSeconds: 0, successThreshold: 0, timeoutSeconds: 0), name: 'docker-container', resourceLimitCpu: '', resourceLimitMemory: '', resourceRequestCpu: '', resourceRequestMemory: '', ttyEnabled: true),
    containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.14.1', name: 'helm-container', ttyEnabled: true)
  ],
  volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')],
) {
  def GIT_REPOS_URL = "git@gitlab.com:questcode01/frontend.git"
  def REPOS
  def GIT_BRANCH

  def IMAGE_POSFIX = ""
  def IMAGE_NAME = "frontend"
  def IMAGE_VERSION
  def ENVIRONMENT

  def HELM_CHART_NAME = "frontend"
  def HELM_REPO = "questcode"
  def CHARTMUSEUM_URL = "http://helm-chartmuseum:8080"
  def KUBE_NAMESPACE
  def KUBE_NODE_PORT
  def KUBE_NODE_PORT_STAGING = "30080"
  def KUBE_NODE_PORT_PROD = "30081"
  def HELM_DEPLOY_NAME

  node(LABEL_ID) {
    stage('Checkout') {
      echo 'Iniciando Clone do Repositorio'

      REPOS = checkout scm
      GIT_BRANCH = REPOS.GIT_BRANCH

      echo GIT_BRANCH

      if(GIT_BRANCH.equals('master')) {
        KUBE_NAMESPACE = 'prod'
        ENVIRONMENT ='production'
        KUBE_NODE_PORT = KUBE_NODE_PORT_PROD
      } else if(GIT_BRANCH.equals('develop')) {
        KUBE_NAMESPACE = 'staging'
        ENVIRONMENT = 'staging'
        KUBE_NODE_PORT = KUBE_NODE_PORT_STAGING
        IMAGE_POSFIX = "-RC"
      } else {
        echo "Não existe pipeline para a branch ${GIT_BRANCH}"
        exit 0
      }

      IMAGE_VERSION = sh returnStdout: true, script: 'sh read-package-version.sh'
      IMAGE_VERSION = IMAGE_VERSION.trim() + IMAGE_POSFIX
    }

    stage('Package') {
      container('docker-container') {
        echo 'Iniciando empacotamento com Docker'

        withCredentials([usernamePassword(credentialsId: 'Dockerhub', passwordVariable: 'DOCKER_HUB_PASSWORD', usernameVariable: 'DOCKER_HUB_USER')]) {
            sh """docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
                  docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV='${ENVIRONMENT}'
                  docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}
               """
          }
      }
    }

    stage('Deploy') {
      echo 'Iniciando Deploy com Helm'

      HELM_DEPLOY_NAME = "${KUBE_NAMESPACE}-${HELM_CHART_NAME}"
      HELM_PATH = "${HELM_REPO}/${HELM_CHART_NAME}"

      container('helm-container') {
        sh """helm init --client-only
              helm repo add ${HELM_REPO} ${CHARTMUSEUM_URL}
              helm repo update
           """
        try {
          sh "helm upgrade --namespace ${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_PATH} --set image.tag=${IMAGE_VERSION} --set service.nodePort=${KUBE_NODE_PORT}"
        } catch(Exception e) {
          sh "helm install --name ${HELM_DEPLOY_NAME} --namespace ${KUBE_NAMESPACE} ${HELM_PATH} --set image.tag=${IMAGE_VERSION} --set service.nodePort=${KUBE_NODE_PORT}"
        }
      }
    }
  }
}
